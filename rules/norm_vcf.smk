rule norm_vcf:
    input:
        vcf = os.path.join(DATAFOLDER["variant_calling"], "{sample}", "{sample}.unnormed.vcf"),
        ref = REFERENCE,
        ref_index = REFERENCE + ".fai"
    output:
        vcf = temp(os.path.join(DATAFOLDER["variant_calling"], "{sample}", "{sample}.vcf"))
    log:
        os.path.join(DATAFOLDER["logs"], "variant_calling", "{sample}.norm.log")
    conda:
        "../envs/bcftools.yaml"
    threads: 1
    shell:
        r"""
            bcftools norm -f {input.ref} -o {output.vcf} {input.vcf} 2> {log}
        """
