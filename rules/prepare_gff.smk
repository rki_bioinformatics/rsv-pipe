rule prepareGFF:
    input:
        ref = str(config['gff'])
    output:
        ref = GFF if GFF else ""
    conda:
        "../envs/dos2unix.yaml"
    shell:
        r"""
            mkdir -p $(dirname "{output.gff}")
            cat {input.gff} > {output.gff}
            dos2unix {output.gff}
        """
