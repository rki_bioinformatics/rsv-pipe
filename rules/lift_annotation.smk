# check, if variants of interest locate to low coverage regions

rule lift_annotation:
	input:
		ref = REFERENCE,
		cns = os.path.join("{path}", "{sample}.{masking}_consensus.fasta"),
		gff = GFF
	output:
		gff = os.path.join("{path}", "{sample}.{masking}_consensus.gff"),
		fai = temp(os.path.join("{path}", "{sample}.{masking}_consensus.fasta.fai")),
		mmi = temp(os.path.join("{path}", "{sample}.{masking}_consensus.fasta.mmi"))
	conda:
		"../envs/liftoff.yaml"
	params:
		unmapped = os.path.join(DATAFOLDER["annotation"], "{sample}", "{sample}.{masking}_consensus.unmapped_features.txt") if "annotation" in DATAFOLDER else "None",
		log = os.path.join(DATAFOLDER["logs"], "annotation", "{sample}.{masking}_consensus.annotation_lift.log")
	shell:
		r"""
			datafolder="$(dirname {params.unmapped})"
			mkdir -p "${{datafolder}}"
			mkdir -p "$(dirname {params.log})"
			liftoff -o {output.gff} -dir "${{datafolder}}"\
					-u {params.unmapped} -g {input.gff} \
					{input.cns} {input.ref}  2> {params.log}
		"""
