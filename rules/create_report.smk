def input_createReport(wildcards):
	files = {
		'coverage': [os.path.join(DATAFOLDER["mapping_stats"], x, x + ".coverage.tsv") for x in SAMPLES],
		'frag_size': [os.path.join(DATAFOLDER["mapping_stats"], x, x + ".fragsize.tsv") for x in SAMPLES],
		'mapping_statistics': [os.path.join(DATAFOLDER["mapping_stats"], x, x + ".bamstats.txt") for x in SAMPLES],
		'mapping_statistics_forR': [os.path.join(DATAFOLDER["mapping_stats"], x, x + ".bamstats.pipe.txt") for x in SAMPLES],
		'version': os.path.join(PROJFOLDER, "pipeline.version")
		}
	if PCR_DEDUP:
		files['dedupped_mapping_statistics'] = [os.path.join(DATAFOLDER["mapping_stats"], x, x + ".dedupped.bamstats.txt") for x in SAMPLES]
		files['dedupped_mapping_statistics_forR'] = [os.path.join(DATAFOLDER["mapping_stats"], x, x + ".dedupped.bamstats.pipe.txt") for x in SAMPLES]
	return files

rule createReport:
    input: unpack(input_createReport)
    output:
        report = os.path.join(PROJFOLDER, "qc_report.html"),
        csv = os.path.join(DATAFOLDER["reporting"], "coverage_samples.csv")
    params:
        p_folder = PROJFOLDER,
        l_folder = DATAFOLDER["reporting"],
        run_id = REPORT_RUNID,
        tax_id = KRAKEN_TAX_ID,
        dedup = PCR_DEDUP,
        template = srcdir("../minipipe.Rmd")
    log:
        os.path.join(DATAFOLDER["logs"], "reporting", "reporting.log")
    conda:
        "../envs/r.yaml"
    threads:
        1
    shell:
        # maybe need to replace shell by r call
        r"""
            # create report
            echo "####### compiling report" >> {log}
            VERSION=$(cat {input.version})
            Rscript -e "rmarkdown::render('{params.template}',
                                            params=list(proj_folder='{params.p_folder}', list_folder='{params.l_folder}', run_name='{params.run_id}', tax_id='{params.tax_id}', dedup='{params.dedup}', version='$VERSION'),
                                            output_file=file.path('{output.report}'))" &> {log}
        """
