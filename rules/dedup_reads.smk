rule dedupReads:
	input:
		bam = os.path.join(DATAFOLDER["mapping"], "{sample}", "{sample}.sort.bam")
	output:
		metric = os.path.join(DATAFOLDER["dedup"], "{sample}", "{sample}.dedup.txt"),
		dedupped_bam = os.path.join(DATAFOLDER["dedup"], "{sample}", "{sample}.dedupped.bam"),
	log:
		os.path.join(DATAFOLDER["logs"], "dedup", "{sample}.dedup.log")
	conda:
		"../envs/picard.yaml"
	threads: 1
	shell:
		r"""
			picard MarkDuplicates INPUT={input.bam} OUTPUT={output.dedupped_bam} METRICS_FILE={output.metric} REMOVE_DUPLICATES=true &>> {log};
        """

			# picard -Xmx10g SortSam INPUT={input.bam} OUTPUT={output.sortedbam} SORT_ORDER=coordinate &> {log};
			# --ASSUME_SORT_ORDER coordinate
