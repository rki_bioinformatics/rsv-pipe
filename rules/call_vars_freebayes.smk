rule callGenomicVariants_freebayes:
    input:
        bam = os.path.join(DATAFOLDER["mapping"], "{sample}", "{sample}.sort.bam") if not PCR_DEDUP else os.path.join(DATAFOLDER["dedup"], "{sample}", "{sample}.dedupped.bam"),
        ref = REFERENCE,
        ref_index = REFERENCE + ".fai"
    output:
        vcf = temp(os.path.join(DATAFOLDER["variant_calling"], "{sample}", "{sample}.unnormed.vcf"))
    params:
        cov = VAR_CALL_COV,
        vcount = VAR_CALL_COUNT,
        frac = VAR_CALL_FRAC,
    log:
        os.path.join(DATAFOLDER["logs"], "variant_calling", "{sample}.call.log")
    conda:
        "../envs/freebayes.yaml"
    threads: 1
    shell:
        r"""
            (freebayes -f {input.ref} --min-alternate-count {params.vcount} --min-alternate-fraction {params.frac} --min-coverage {params.cov} --pooled-continuous --haplotype-length -1 {input.bam} > {output.vcf}) 2> {log}
        """
