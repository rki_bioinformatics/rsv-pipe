def input_createReport(wildcards):
	path = DATAFOLDER["dedup"] if wildcards.fname.endswith(".dedupped") else DATAFOLDER["mapping"]
	return { "bam": os.path.join(path, "{fname}.bam") }

rule getBamStats:
    input:
        unpack(input_createReport)
    output:
        stats = os.path.join(DATAFOLDER["mapping_stats"], "{fname}.bamstats.txt"),
        stats_forR = os.path.join(DATAFOLDER["mapping_stats"], "{fname}.bamstats.pipe.txt")
    log:
        os.path.join(DATAFOLDER["logs"], "mapping_stats", "{fname}.bamstats.log")
    conda:
        "../envs/samtools.yaml"
    shell:
        r"""
            samtools flagstat {input.bam} 1> {output.stats} 2> {log};
            cat {output.stats} | sed -e 's/ + /|/' | sed -e 's/ /|/' 1> {output.stats_forR} 2> {log}
        """
